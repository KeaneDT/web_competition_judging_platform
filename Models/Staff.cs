﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10202630.Models
{
    public class Staff
    {
        //StaffID
        [Display(Name = "ID")]
        public int StaffId { get; set; }
        //Name
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        //Gender
        public char Gender { get; set; }
        //DOB
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }
        //Nationality
        public string Nationality { get; set; }
        //Email
        [Display(Name = "Email Address")]
        [EmailAddress] // Validation Annotation for email address format
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }
        //Salary
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00)]
        public decimal Salary { get; set; }
        //IsFullTime
        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }
        //BranchNo
        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }
    }
}
